#!/bin/bash

LIBWINE="deepin-libwine_2.18-10_i386.deb"
WINE32="deepin-wine32_2.18-10_i386.deb"
WINE32PRELOADER="deepin-wine32-preloader_2.18-10_i386.deb"
WINE32TOOL="deepin-wine32-tools_2.18-10_i386.deb"
WINE="deepin-wine_2.18-10_all.deb"
WINEBINFMT="deepin-wine-binfmt_2.18-10_all.deb"

UDIS86="udis86-1.7.2-i586-1_slonly.txz"
P7ZIP="p7zip-full_16.02+dfsg-6_i386.deb"


echo "installing $LIBWINE"
mkdir LIBWINE
cd LIBWINE
ar x ../$LIBWINE
tar xf data.tar.xz
#rm usr/share -r
cp usr / -r
cd ..

echo "installing $WINE32"
mkdir WINE32
cd WINE32
ar x ../$WINE32
tar xf data.tar.xz
#rm usr/share -r
cp usr / -r
cd ..

echo "installing $WINE32PRELOADER"
mkdir WINE32PRELOADER
cd WINE32PRELOADER
ar x ../$WINE32PRELOADER
tar xf data.tar.xz
#rm usr/share -r
cp usr / -r
cd ..


echo "installing $WINE32TOOL"
mkdir WINE32TOOL
cd WINE32TOOL
ar x ../$WINE32TOOL
tar xf data.tar.xz
#rm usr/share -r
cp usr / -r
cd ..


echo "installing $WINE"
mkdir WINE
cd WINE
ar x ../$WINE
tar xf data.tar.xz
#rm usr/share -r
cp usr / -r
cd ..


echo "installing $WINEBINFMT"
mkdir WINEBINFMT
cd WINEBINFMT
ar x ../$WINEBINFMT
tar xf data.tar.xz
#rm usr/share -r
cp usr / -r
cd ..



echo "installing $UDIS86"
mkdir udis86
cd udis86
tar xf ../$UDIS86
cp usr/lib/libudis86.so.0.0.0 /usr/lib/libudis86.so.0
cd ..

echo "installing $P7ZIP"
mkdir p7zip
cd p7zip
ar x ../$P7ZIP
tar xf data.tar.xz
rm usr/share -r
cp usr / -r
cd ..

echo "done"

exit 0
