# com.deepin.wine

#### Description
为了避免下载链接由于软件库的更新而失效，将构建deepin-wine运行时所需要的文件保存在本仓库内


各个文件的原始下载链接：    
deepin-libwine_2.18-10_i386.deb:
http://mirrors.ustc.edu.cn/deepin/pool/non-free/d/deepin-wine/deepin-libwine_2.18-10_i386.deb   
deepin-wine32_2.18-10_i386.deb:
http://mirrors.ustc.edu.cn/deepin/pool/non-free/d/deepin-wine/deepin-wine32_2.18-10_i386.deb    
udis86-1.7.2-i586-1_slonly.txz:
https://packages.slackonly.com/pub/packages/14.2-x86/development/udis86/udis86-1.7.2-i586-1_slonly.txz  


请检查下载链接的内容与仓库内文件的sha512sum（或其他校验值）是否一致      
若下载链接失效，请从对应的链接下载更新的版本，然后修改manifest文件用更新的版本构建


声明：     
该厂库所有二进制文件全部来源于公共下载地址，本人未对其进行过二次修改和打包，但该保证无任何法律效力。所有非二进制文本文件均可自由查阅。     
因为使用本仓库的任何内容所导致的任何后果与本人无关，若你无法对使用该厂库后的任何后果负责，请不要使用本仓库的任何内容。     
我不拥有该厂库任何二进制文件的版权，所有由本人编写的非二进制文件以GPL开源协议开源。若该仓库的文件侵犯了您的法律权益，请联系<wsgalaxy@qq.com>，我会删除侵权内容并道歉。       
该项目得以实现的全部功劳来自于深度操作系统开发人员的辛勤努力，本人只是将其成果适配到flatpak平台以让这一伟大成果能为更多人所共享，本人对深度操作系统的开发人员致以崇高的敬意。      


